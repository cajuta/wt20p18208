var Modul = (function() {
    var iscrtajRaspored = function(div, dani, satPocetak, satKraj) {
        if (satPocetak >= satKraj || satPocetak < 0 || satPocetak > 24 ||
            satKraj < 0 || satKraj > 24 || !Number.isInteger(satPocetak) || !Number.isInteger(satKraj)) {
            var p = document.createElement("p");
            var t = document.createTextNode("Greška!");
            p.appendChild(t);
            div.appendChild(p);
        } else {
            if (div.getElementsByTagName("table").length > 0) { // zabrana crtanja vise tabela u jednom divu
                div.getElementsByTagName("table")[0].parentNode.removeChild(div.getElementsByTagName("table")[0]);
            }
            var table = document.createElement("table");
            var tbdy = document.createElement('tbody');
            var brojKolona = (satKraj - satPocetak) * 2;
            for (var i = 0; i <= dani.length; i++) {
                var tr = document.createElement('tr');
                if (i == 0) { // postavljanje reda za sate
                    for (var j = 0; j <= ((brojKolona) / 2); j++) {
                        var td = document.createElement('td');
                        if (j != 0) {
                            var sat = j - 1 + satPocetak;
                            td.setAttribute('colspan', '2');
                            if (j == 1) {
                                var tekst = document.createTextNode(sat + ":00");
                                td.appendChild(tekst);
                            } else if ((sat <= 12 && sat % 2 == 0) || (sat >= 15 && sat % 2 == 1)) {
                                var tekst = document.createTextNode(sat + ":00");
                                td.appendChild(tekst);
                            }
                        }
                        if (j == 1) { //id prvog sata
                            td.id = satPocetak;
                        } else if (j == brojKolona / 2) {
                            td.id = satKraj;
                        }
                        td.className = "sati";
                        tr.appendChild(td);
                    }
                } else {
                    for (var z = 0; z <= brojKolona; z++) {
                        var td = document.createElement('td');
                        if (z == 0) { //upisivanje dana 
                            td.style.border = "none";
                            var dan = document.createTextNode(dani[i - 1]);
                            tr.id = dani[i - 1] + div.id; //postavljanje id reda svake tabele
                            td.appendChild(dan);
                            td.id = "dani";
                        } else if (z % 2 == 1) {
                            td.className = "isprekidano";
                        } else {
                            td.className = "puna";
                        }
                        tr.appendChild(td);
                    }
                }
                tbdy.appendChild(tr);
            }
            table.appendChild(tbdy);
            div.appendChild(table);
        }
    }

    var dodajAktivnost = function(raspored, naziv, tip, vrijemePocetak, vrijemeKraj, dan) {
        var tabele = raspored.getElementsByTagName("table");
        var tabela = tabele[tabele.length - 1];

        if (tabele.length == 0) {
            alert("Greška - raspored nije kreiran");
            return "Greška - raspored nije kreiran";
        }

        var duzina = (vrijemeKraj - vrijemePocetak) * 2;
        var red = document.getElementById(dan + raspored.id);
        var satPocetak = tabela.getElementsByTagName("td")[1].id;
        var satKraj = tabela.getElementsByTagName("tr")[0].lastChild.id;
        if (red == null || vrijemePocetak < satPocetak || vrijemeKraj > satKraj || vrijemePocetak > vrijemeKraj) {
            alert("Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin");
            return "Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin";
        }
        red = red.rowIndex;
        var kolona = (vrijemePocetak - satPocetak) * 2 + 1;
        var pocetniBrojKolona = (satKraj - satPocetak) * 2 + 1;
        var colspan = 0;
        var pozicija = kolona;
        var brojKolona = tabela.getElementsByTagName("tr")[red].childElementCount;
        var atribut = tabela.getElementsByTagName("tr")[red].childNodes[kolona].className; //className prve celije aktivnosti

        if (pocetniBrojKolona > brojKolona) { //slucaj kada u redu vec postoji druga aktivnost i pokusava se dodati aktivnost iza
            for (var i = 1; i < kolona; i++) {
                if (tabela.getElementsByTagName("tr")[red].childNodes[i].colSpan > 1) {
                    colspan = tabela.getElementsByTagName("tr")[red].childNodes[i].colSpan;
                    pozicija = pozicija - colspan + 1;
                }
            }
            kolona = pozicija;
        }

        for (var j = kolona; j <= kolona + duzina - 1; j++) { //provjera da li se preklapaju aktivnosti
            if (j < brojKolona) {
                if (tabela.getElementsByTagName("tr")[red].childNodes[j].id == "aktivnost" ||
                    tabela.getElementsByTagName("tr")[red].childNodes[j].id == "aktivnostIsprekidano") {
                    alert("Greška - već postoji termin u rasporedu u zadanom vremenu");
                    return "Greška - već postoji termin u rasporedu u zadanom vremenu";
                }
            }
        }

        for (var j = kolona; j < kolona + duzina - 1; j++) { //brisanje nepotrebnih celija
            tabela.getElementsByTagName("tr")[red].deleteCell(kolona);
        }


        var aktivnost = tabela.getElementsByTagName("tr")[red].childNodes[kolona]; //dodavanje aktivnosti
        aktivnost.setAttribute('colspan', duzina);

        if (atribut == "puna") {
            aktivnost.id = "aktivnostIsprekidano";
        } else {
            aktivnost.id = "aktivnost";
        }
        var tekst = document.createTextNode(naziv);
        aktivnost.appendChild(tekst);
        var br = document.createElement("br");
        aktivnost.appendChild(br);
        tekst = document.createTextNode(tip);
        aktivnost.appendChild(tekst);

    }


    return {
        iscrtajRaspored: iscrtajRaspored,
        dodajAktivnost: dodajAktivnost,
    }
}());