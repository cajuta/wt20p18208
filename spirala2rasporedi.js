let okvir = document.getElementById("okvir");
let drugiOkvir = document.getElementById("drugiOkvir");

iscrtajRaspored(okvir, ['Ponedjeljak', 'Utorak', 'Srijeda', 'Četvrtak', 'Petak'], 8, 20);
dodajAktivnost(okvir, 'RMA', 'predavanje', 12, 13.5, 'Ponedjeljak');
dodajAktivnost(okvir, 'RMA', 'predavanje', 14, 17, 'Ponedjeljak');
dodajAktivnost(okvir, 'RMA', 'vježbe', 12.5, 14, 'Utorak');
dodajAktivnost(okvir, 'DM', 'tutorijal', 14, 16, 'Utorak');
dodajAktivnost(okvir, 'OI', 'predavanje', 12, 15, 'Srijeda');
dodajAktivnost(okvir, 'DM', 'predavanje', 13, 16, 'Utorak');
dodajAktivnost(okvir, 'WT', 'predavanje', 10, 12.5, 'Petak');
dodajAktivnost(okvir, 'WT', 'vjezbe', 14, 17, 'Petak');
dodajAktivnost(okvir, 'RG', 'predavanje', 9, 11, 'Četvrtak');
dodajAktivnost(okvir, 'RG', 'vjezbe', 15, 17, 'Četvrtak');




iscrtajRaspored(drugiOkvir, ['Ponedjeljak', 'Utorak', 'Srijeda', 'Četvrtak', 'Petak'], 10, 20);
dodajAktivnost(drugiOkvir, 'OOI', 'vježbe', 11, 13, 'Srijeda');
dodajAktivnost(drugiOkvir, 'RMA', 'vježbe', 14, 15, 'Srijeda');
dodajAktivnost(drugiOkvir, 'OOI', 'vježbe', 12, 13, 'Utorak');
dodajAktivnost(drugiOkvir, 'RMA', 'vježbe', 14, 15, 'Ponedjeljak');
dodajAktivnost(drugiOkvir, 'OOI', 'vježbe', 19, 20, 'Petak');
dodajAktivnost(drugiOkvir, 'RMA', 'vježbe', 17, 19, 'Srijeda');
dodajAktivnost(drugiOkvir, 'OOI', 'vježbe', 10, 13, 'Četvrtak');
dodajAktivnost(drugiOkvir, 'RMA', 'vježbe', 11, 15, 'Petak');
dodajAktivnost(drugiOkvir, 'OOI', 'vježbe', 11, 13, 'Ponedjeljak');
dodajAktivnost(drugiOkvir, 'RMA', 'vježbe', 14, 15, 'Utorak');