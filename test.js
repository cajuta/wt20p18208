let assert = chai.assert;
describe('Raspored', function() {
    describe('Crtanje tabele', function() {
        it('Treba imati 6 redova za 5 dana (ukljucujemo red za sate).', function() {
            Modul.iscrtajRaspored(okvir, ['Ponedjeljak', 'Utorak', 'Srijeda', 'Četvrtak', 'Petak'], 8, 21);
            let tabele = okvir.getElementsByTagName("table");
            let tabela = tabele[tabele.length - 1];
            let redovi = tabela.getElementsByTagName("tr");
            assert.equal(redovi.length, 6, "Broj redova treba biti 6");
        });

        it('Treba imati (satKraj-satPocetak)*2+1 kolona.', function() {
            Modul.iscrtajRaspored(test1, ['Ponedjeljak', 'Utorak', 'Srijeda', 'Četvrtak', 'Petak'], 8, 19);
            let tabele = test1.getElementsByTagName("table");
            let tabela = tabele[tabele.length - 1];
            let red = tabela.getElementsByTagName("tr")[1];
            assert.lengthOf(red.cells, 23, "Broj kolona treba biti 23");
        });

        it('Red za sate ima u pola manje kolona, nego ostale.', function() {
            Modul.iscrtajRaspored(test2, ['Ponedjeljak', 'Utorak', 'Srijeda', 'Četvrtak', 'Petak'], 10, 19);
            let tabele = test2.getElementsByTagName("table");
            let tabela = tabele[tabele.length - 1];
            let red = tabela.getElementsByTagName("tr")[0];
            assert.lengthOf(red.cells, 10, "Broj kolona treba biti 10");
        });

        it('Ispis greške kada je satPocetak veci od satKraj.', function() {
            Modul.iscrtajRaspored(test3, ['Ponedjeljak', 'Utorak', 'Srijeda', 'Četvrtak', 'Petak'], 19, 10);
            let paragrafi = test3.getElementsByTagName("p");
            let paragraf = paragrafi[paragrafi.length - 1];
            let tekst = "Greška!";
            assert.equal(tekst, paragraf.innerHTML, "Ispisuje se Greška!");
        });

        it('Ispis greške kada je satPocetak negativan.', function() {
            Modul.iscrtajRaspored(test4, ['Ponedjeljak', 'Utorak', 'Srijeda', 'Četvrtak', 'Petak'], -10, 19);
            let paragrafi = test4.getElementsByTagName("p");
            let paragraf = paragrafi[paragrafi.length - 1];
            let tekst = "Greška!";
            assert.equal(tekst, paragraf.innerHTML, "Ispisuje se Greška!");
        });

        it('Ispis greške kada je satKraj negativan.', function() {
            Modul.iscrtajRaspored(test5, ['Ponedjeljak', 'Utorak', 'Srijeda', 'Četvrtak', 'Petak'], 10, -19);
            let paragrafi = test5.getElementsByTagName("p");
            let paragraf = paragrafi[paragrafi.length - 1];
            let tekst = "Greška!";
            assert.equal(tekst, paragraf.innerHTML, "Ispisuje se Greška!");
        });

        it('Ispis greške kada satPocetak nije cijeli broj.', function() {
            Modul.iscrtajRaspored(test6, ['Ponedjeljak', 'Utorak', 'Srijeda', 'Četvrtak', 'Petak'], 9.7, 20);
            let paragrafi = test6.getElementsByTagName("p");
            let paragraf = paragrafi[paragrafi.length - 1];
            let tekst = "Greška!";
            assert.equal(tekst, paragraf.innerHTML, "Ispisuje se Greška!");
        });

        it('Ispis greške kada satKraj nije cijeli broj.', function() {
            Modul.iscrtajRaspored(test7, ['Ponedjeljak', 'Utorak', 'Srijeda', 'Četvrtak', 'Petak'], 8, 10.5);
            let paragrafi = test7.getElementsByTagName("p");
            let paragraf = paragrafi[paragrafi.length - 1];
            let tekst = "Greška!";
            assert.equal(tekst, paragraf.innerHTML, "Ispisuje se Greška!");
        });

        it('Ispis greške kada satKraj je veći od 24.', function() {
            Modul.iscrtajRaspored(test8, ['Ponedjeljak', 'Utorak', 'Srijeda', 'Četvrtak', 'Petak'], 10, 30);
            let paragrafi = test8.getElementsByTagName("p");
            let paragraf = paragrafi[paragrafi.length - 1];
            let tekst = "Greška!";
            assert.equal(tekst, paragraf.innerHTML, "Ispisuje se Greška!");
        });

        it('Red za sate ispisuje samo odredjene vrijednosti.', function() {
            Modul.iscrtajRaspored(test9, ['Ponedjeljak', 'Utorak', 'Srijeda', 'Četvrtak', 'Petak'], 12, 20);
            let tabele = test9.getElementsByTagName("table");
            let tabela = tabele[tabele.length - 1];
            let red = tabela.getElementsByTagName("tr")[0];
            let sati = [];
            for (var i = 0; i < red.cells.length; i++) {
                sati.push(red.childNodes[i].innerHTML);
            }
            let nedozvoljeniSat = "14:00"
            assert.equal(false, sati.includes(nedozvoljeniSat), "Ne sadrži nedozvoljeni sat.");
        });


    });

    describe('Dodavanje aktivnosti', function() {
        this.timeout(1000000);
        it('Dodavanje jedne aktivnosti.', function() {
            Modul.iscrtajRaspored(test10, ['Ponedjeljak', 'Utorak', 'Srijeda', 'Četvrtak', 'Petak'], 8, 21);
            Modul.dodajAktivnost(test10, 'PWS', 'predavanje', 11.5, 12.5, 'Ponedjeljak');
            let duzina = (12.5 - 11.5) * 2;
            let kolona = (11.5 - 8) * 2 + 1;
            let tabele = test10.getElementsByTagName("table");
            let tabela = tabele[tabele.length - 1];
            let red = tabela.getElementsByTagName("tr")[1];
            let colspan = red.childNodes[kolona].colSpan;
            assert.equal(2, colspan, "Provjera duzine aktivnosti.");

        });

        it('Greška, nije kreirana tabela.', function() {
            let greška = Modul.dodajAktivnost(drugiOkvir, 'RMA', 'predavanje', 11.5, 12.5, 'Ponedjeljak');
            let alert = "Greška - raspored nije kreiran";
            assert.equal(alert, greška, 'Raspored nije kreiran.');
        });

        it('Greška, ne postoji vrijeme aktivnosti u rasporedu.', function() {
            Modul.iscrtajRaspored(test11, ['Ponedjeljak', 'Utorak', 'Srijeda', 'Četvrtak', 'Petak'], 15, 21);
            let alert = "Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin";
            let greška = Modul.dodajAktivnost(test11, 'RMA', 'predavanje', 8, 12.5, 'Ponedjeljak');
            assert.equal(alert, greška, 'Ne postoji vrijeme u rasporedu.');

        });

        it('Greška, ne postoji dan u rasporedu.', function() {
            Modul.iscrtajRaspored(test12, ['Ponedjeljak', 'Utorak', 'Srijeda'], 10, 21);
            let alert = "Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin";
            let greška = Modul.dodajAktivnost(test12, 'OOI', 'vježbe', 12, 15, 'Cetvrtak');
            assert.equal(alert, greška, 'Ne postoji dan u rasporedu');
        });

        it('Greška, vec postoji aktivnost u tom terminu - poslije.', function() {
            Modul.iscrtajRaspored(test13, ['Ponedjeljak', 'Utorak', 'Srijeda', 'Četvrtak', 'Petak'], 8, 21);
            let alert = "Greška - već postoji termin u rasporedu u zadanom vremenu";
            Modul.dodajAktivnost(test13, 'VVS', 'vjezbe', 12, 15, 'Ponedjeljak');
            let greška = Modul.dodajAktivnost(test13, 'RMA', 'predavanje', 14, 17, 'Ponedjeljak');
            assert.equal(alert, greška, 'Preklapanje aktivnosti');

        });

        it('Greška, vec postoji aktivnost u tom terminu - prije.', function() {
            Modul.iscrtajRaspored(test14, ['Ponedjeljak', 'Utorak', 'Srijeda', 'Četvrtak', 'Petak'], 8, 21);
            let alert = "Greška - već postoji termin u rasporedu u zadanom vremenu";
            Modul.dodajAktivnost(test14, 'VVS', 'predavanja', 12, 15, 'Ponedjeljak');
            let greška = Modul.dodajAktivnost(test14, 'RMA', 'predavanje', 10, 13, 'Ponedjeljak');
            assert.equal(alert, greška, 'Preklapanje aktivnosti');

        });

        it('Aktivnosti dodane na isti dan.', function() {
            Modul.iscrtajRaspored(test15, ['Ponedjeljak', 'Utorak', 'Srijeda', 'Četvrtak', 'Petak'], 8, 21);
            Modul.dodajAktivnost(test15, 'WT', 'predavanja', 10, 12, 'Ponedjeljak');
            Modul.dodajAktivnost(test15, 'WT', 'vjezbe', 15, 18, 'Ponedjeljak');
            let tabele = document.getElementsByTagName("table");
            let tabela = tabele[tabele.length - 1];
            let red = tabela.getElementsByTagName("tr")[1];
            assert.lengthOf(red.cells, 19, 'Smanjen broj kolona u zavisnosti od predavanja');
        });

        it('Aktivnosti dodane jedna do druge.', function() {
            Modul.iscrtajRaspored(test16, ['Ponedjeljak', 'Utorak', 'Srijeda', 'Četvrtak', 'Petak'], 8, 21);
            Modul.dodajAktivnost(test16, 'OIS', 'predavanja', 10, 12, 'Ponedjeljak');
            Modul.dodajAktivnost(test16, 'OIS', 'vjezbe', 12, 14, 'Ponedjeljak');
            let tabele = test16.getElementsByTagName("table");
            let tabela = tabele[tabele.length - 1];
            let red = tabela.getElementsByTagName("tr")[1];
            let aktivnost1 = red.childNodes[5].id;
            let aktivnost2 = red.childNodes[6].id;
            if (aktivnost1 == "aktivnostIsprekidano") {
                aktivnost1 = "aktivnost";
            }
            if (aktivnost2 == "aktivnostIsprekidano") {
                aktivnost2 = "aktivnost";
            }

            assert.equal(aktivnost1, aktivnost2, "Pravilno dodane jedna do druge.");
        });

        it('Aktivnost dodana na kraj rasporeda.', function() {
            Modul.iscrtajRaspored(test17, ['Ponedjeljak', 'Utorak', 'Srijeda', 'Četvrtak', 'Petak'], 8, 21);
            Modul.dodajAktivnost(test17, 'WT', 'predavanja', 19, 21, 'Srijeda');
            let tabele = test17.getElementsByTagName("table");
            let tabela = tabele[tabele.length - 1];
            let zadnja = tabela.getElementsByTagName("tr")[3].lastChild;
            assert.equal(zadnja.colSpan, 4, 'Aktivnost dodana na kraj.');
        });

        it('Aktivnost dodana na početak rasporeda.', function() {
            Modul.iscrtajRaspored(test18, ['Ponedjeljak', 'Utorak', 'Srijeda', 'Četvrtak', 'Petak'], 8, 21);
            Modul.dodajAktivnost(test18, 'RG', 'predavanja', 8, 15, 'Srijeda');
            let tabele = test18.getElementsByTagName("table");
            let tabela = tabele[tabele.length - 1];
            let prva = tabela.getElementsByTagName("tr")[3].childNodes[1];
            assert.equal(prva.colSpan, 14, 'Aktivnost dodana na početak.');
        });

    });
});